﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class RayCast : MonoBehaviour
{	


	public const int COUNT_DOORS = 6;
	private List<bool> listStatusDoor = new List<bool>(COUNT_DOORS);

	public bool getStatusDoor(int index){
		return listStatusDoor[index];
	}

	public void openDoorCloseOther(int index){
		for(int j = 0; j< COUNT_DOORS; j++){
			
			if(j == index && listStatusDoor[j]==false){
				listStatusDoor[j] = true;
				PlaySoundOpenDoor();
			}else if(j != index && listStatusDoor[j]==true){
				listStatusDoor[j] = false;

			}

		}
	}

	public void setStatusDoors(bool value){
		for(int j = 0; j< COUNT_DOORS; j++){
			listStatusDoor[j] = value;
		}
	}

	public List<bool> getList(){
		return listStatusDoor;
	}

	public Light light;
	public AudioClip dfs;
	public GameObject af;
	public AudioClip dos;
	public GameObject ao;
	public int b = 0;
	public int c = 0;
	public int d = 0;
	public int g = 0;
	public int g1 = 0;
	public int e1 = 0;
	//public int f1 = 0;
	public int e = 0;
	public int f = 0;
	private int rand = 0;
	private int a = 0;
	public GameObject cube;
	public int tr = -2;
	private int col = 0;
	public GameObject[] block = new GameObject[6];
	private float dist = 8f;
	private int value = 0;
	private HashSet <string> ignoreName = new HashSet<string>();
	public GameObject bone;
	public bool isBone = false;
	private int click = 0;
	private int click1 = 2;
	private int click2 = 4;
	private int click3 = 6;


	public RayCast() {


		ignoreName.Add("Cube");
		ignoreName.Add("Number");
		ignoreName.Add("Window");
		ignoreName.Add("Ladders");
		ignoreName.Add("Ed");
		ignoreName.Add("Tile");
		ignoreName.Add("Tile (1)");
		ignoreName.Add("Tile (2)");
		ignoreName.Add("Tile (3)");
		ignoreName.Add("Trigger");
		ignoreName.Add("BlockDoor");
		ignoreName.Add("BlockDoor1");
		ignoreName.Add("BlockDoor2");
		ignoreName.Add("BlockDoor3");
		ignoreName.Add("BlockDoor4");
		ignoreName.Add("BlockDoor5");
		ignoreName.Add("Bone");


	}



	void Start ()
	{   

		for(int j = 0; j<COUNT_DOORS; ++j){
			listStatusDoor.Add(false);
		}
			
	}


	void Update ()
	{

			rayCaster();

	}


	void rayCaster() {
		RaycastHit hit;

		Ray ray = new Ray (transform.position, transform.forward);

		Debug.DrawRay (transform.position, transform.forward * dist);

		if (Physics.Raycast (ray, out hit, dist)) {//
			if (Input.GetMouseButtonDown (0)) {
				Debug.Log(hit.point);
			if (hit.collider.gameObject.name == "Bone") {
					Debug.Log(hit.point);

					if (hit.point.x <= -0.4) {
						click ++;

					hit.collider.gameObject.GetComponent<Animator>().SetInteger("RotateX",click);
						if (click==2) {
							click = 0;

						}
					}

					if (hit.point.x >0.4) {
						click1 ++;

						hit.collider.gameObject.GetComponent<Animator>().SetInteger("RotateX",click1);
						if (click1==4) {
							click1 = 2;

						}
					}

					if (hit.point.z <= -0.4) {
						click2 ++;

						hit.collider.gameObject.GetComponent<Animator>().SetInteger("RotateX",click2);
						if (click2==6) {
							click2 = 4;

						}
					}

					if (hit.point.z >0.4) {
						click3 ++;

						hit.collider.gameObject.GetComponent<Animator>().SetInteger("RotateX",click3);
						if (click3==8) {
							click3 = 6;

						}
					}

						

				



				} 
			}
			if (!ignoreName.Contains(hit.collider.name)) {
			

				col = int.Parse(hit.collider.name)-1;
				if(cube.GetComponent<WhatIsCube>().text[col].color != Color.red && cube.GetComponent<WhatIsCube>().text[col].color != Color.green){
				cube.GetComponent<WhatIsCube>().text[col].color = new Color(1.0f,0.5f,0.0f,1.0f);
				}
			if (Input.GetMouseButtonDown (0)) {
					
				
				tr = int.Parse(hit.collider.gameObject.name)-1;
				value = int.Parse(hit.collider.gameObject.GetComponent<Text>().text);
				chekAnswer ();
					Debug.Log ("value ="+value+"tr ="+tr);
				}
		
			} else {
				if(cube.GetComponent<WhatIsCube>().text[col].color != Color.red && cube.GetComponent<WhatIsCube>().text[col].color != Color.green){
				cube.GetComponent<WhatIsCube>().text[col].color = new Color(0.208f, 0.122f, 0.043f, 1.0f);
				}
			}
				
		}//
	

	}


	void chekAnswer() {


		if(value%3 == 0 && tr<=7) {

			openDoorCloseOther(0);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.green;

		}  else if (value%3 != 0 && tr<=7) {
			
			blockDoor (0);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.red;
		}

		if (value == b * c && tr <= 47 && tr > 39) {


			openDoorCloseOther (5);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.green;

		} else if (value != b * c && tr <= 47 && tr > 39) {

			blockDoor (5);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.red;
		}

		if (value % 5 == 0 && tr <= 15 && tr > 7) {

			openDoorCloseOther (1);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.green;

		} else if (value % 5 != 0 && tr <= 15 && tr > 7) {
			blockDoor (1);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.red;

		}

		if (value == d && tr <= 31 && tr > 23) {

			openDoorCloseOther (3);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.green;

		} else if (value != d && tr <= 31 && tr > 23) {
			blockDoor (3);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.red;
		}

		if (value == g && tr <= 39 && tr > 31) {


			openDoorCloseOther (4);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.green;

		} else if (value != g && tr <= 39 && tr > 31) {
			blockDoor (4);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.red;
		}

		if (value == g1 && tr <= 23 && tr > 15) {

			openDoorCloseOther (2);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.green;

		} else if (value != g1 && tr <= 23 && tr > 15) {
			blockDoor (2);
			cube.GetComponent<WhatIsCube>().text[col].color = Color.red;
		}

	}

	public void PlaySoundOpenDoor() {
		ao.GetComponent<AudioSource>().clip = dos;
		ao.GetComponent<AudioSource>().enabled = true;
		ao.GetComponent<AudioSource>().Play();
	}

		 void PlaySoundBlockDoor() {
		af.GetComponent<AudioSource>().clip = dfs;
		af.GetComponent<AudioSource>().enabled = true;
		af.GetComponent<AudioSource>().Play();
	}

	void blockDoor(int index) {
		block [index].GetComponent<Animator> ().SetBool("IsBlocked",true);
		PlaySoundBlockDoor ();
	}



	public void randomBlocked(int indexDoor, int indexDoor2){


		do {
			indexDoor = Random.Range (0, 6);
			indexDoor2 = Random.Range (0, 6);
			Debug.Log ("indexdoor ="+indexDoor +" "+indexDoor2);
		} while (indexDoor == indexDoor2);
			
			
			block [indexDoor].GetComponent<Animator> ().SetBool("IsBlocked",true);
			block [indexDoor2].GetComponent<Animator> ().SetBool("IsBlocked",true);
			PlaySoundBlockDoor ();
			Debug.Log("autoblock");
		} 

	public void OnLight() {

		light.GetComponent<Light>().enabled = true;

		}




}
