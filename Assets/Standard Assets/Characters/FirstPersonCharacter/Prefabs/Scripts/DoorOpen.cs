﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DoorOpen : MonoBehaviour {
	
	public GameObject player;
	public GameObject door;
	public GameObject door1;
	public GameObject door2;
	public GameObject door3;
	public GameObject door4;
	public GameObject door5;
	private string nameParam;
	private List<GameObject> listDoors = new List<GameObject> ();

	void Start () {
		listDoors.Add(door);
		listDoors.Add(door1);
		listDoors.Add(door2);
		listDoors.Add(door3);
		listDoors.Add(door4);
		listDoors.Add(door5);
	}
		
	void Update () {
		
			int indexDoor = 0;
			foreach (bool valueDoor in player.GetComponent<RayCast>().getList()) {
				if (indexDoor == 0) {
					nameParam = "";
				} else if (indexDoor != 0) {
					nameParam = indexDoor.ToString ();
				}
			if ((Mathf.Abs (Mathf.Abs(listDoors [indexDoor].transform.position.x) - Mathf.Abs(player.transform.position.x)) < 7.8f) && (Mathf.Abs (Mathf.Abs(listDoors [indexDoor].transform.position.y) - Mathf.Abs(player.transform.position.y)) < 7.8f) && (Mathf.Abs (Mathf.Abs(listDoors [indexDoor].transform.position.z) - Mathf.Abs(player.transform.position.z)) < 7.8f)) {
					
				listDoors [indexDoor].GetComponent<Animator> ().SetBool ("IsDoor" + nameParam, valueDoor);


				}
				indexDoor++;
			}

			
}


}