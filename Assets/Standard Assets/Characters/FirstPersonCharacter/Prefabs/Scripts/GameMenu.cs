﻿using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour {
	private int window ;
	// Use this for initialization
	void Start () {
		window = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () { 
		if (window == 0) { // теперь главное меню активировано  при window = 0 
			GUI.Box (new Rect (Screen.width/2 - 100,Screen.height/2 - 100,200,180), "Главное меню"); 
			if (GUI.Button (new Rect (Screen.width/2 - 90,Screen.height/2 - 80,180,30), "Играть")) { 
				Application.LoadLevel ("Cube"); 
			} 
			if (GUI.Button (new Rect (Screen.width/2 - 90,Screen.height/2 - 40,180,30), "Настройки")) { 
				window = 1; // активируем окно "настройки" 
			} 
			if (GUI.Button (new Rect (Screen.width/2 - 90,Screen.height/2 - 0,180,30), "Помощь")) { 
				window = 2; //активируем окно "помощь" 
			} 
			if (GUI.Button (new Rect (Screen.width/2 - 90,Screen.height/2 + 40,180,30), "Выход")) { 
				Application.Quit(); 
			} 
		} 
		if (window == 1) {  // наши настройки 
			GUI.Box (new Rect (Screen.width/2 - 100,Screen.height/2 - 100,200,180), "Настройки"); 
			if (GUI.Button (new Rect (Screen.width/2 - 90,Screen.height/2 + 40,180,30), "Назад")) { 
				window = 0; 
			} 
		} 
		if (window == 2) { // наша помощь 
			GUI.Box (new Rect (Screen.width/2 - 100,Screen.height/2 - 100,200,180), "Помощь"); 
			if (GUI.Button (new Rect (Screen.width/2 - 90,Screen.height/2 + 40,180,30), "Назад")) { 
				window = 0; 
			} 
		} 
	}
}
