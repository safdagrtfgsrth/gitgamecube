﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WhatIsCube : MonoBehaviour {
	private int b = 0;
	private int c = 0;
	private int d = 0;
	private int g = 0;
	private int g1 = 0;
	private int e1 = 0;
	private int id = 0;
	private int id2 = 0;
	private int e = 0;
	private int f = 0;
	private int rand = 0;
	private int a = 0;
	public Text[] zadanie = new Text[6];
	public Text[] text = new Text[48];
	public GameObject [] block = new GameObject[6];
	public GameObject bone ;
	//public int x = 0;
	//public int y = 0;
	//public int z = 0;


	public Light light;
	public GameObject whatIsCube;
	public GameObject player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col) {
		
		if (col.gameObject.tag=="Player"){
			player.GetComponent<RayCast> ().cube = whatIsCube;
			addAnswer ();
			getBlock ();
			player.GetComponent<RayCast>().bone = bone;
			player.GetComponent<RayCast> ().randomBlocked (id,id2);
			player.GetComponent<RayCast> ().light =light;
			player.GetComponent<RayCast> ().OnLight ();
		

			//int xpos = x;
			//int ypos = y;
			//int zpos = z;


		}
	}
	void addAnswer(){
		//Признак делисости на 3
		a = Random.Range(0,7);
		for(int i = 0; i<=7; i++) {
			rand = Random.Range(10000,99999);
			text[i].text = rand.ToString();
			//text[i].color = new Color(0.208f, 0.122f, 0.043f, 1.0f);

		}

		rand = Random.Range(33000,33333)*3;
		text[a].text = rand.ToString();

		//Признак делимости на 5
		a = Random.Range(8,15);
		for(int i = 8; i<=15; i++) {
			rand = Random.Range(10000,99999);
			text[i].text = rand.ToString();
			//text[i].color = new Color(0.208f, 0.122f, 0.043f, 1.0f);
		}

		rand = Random.Range(2000,19999)*5;
		text[a].text = rand.ToString();

		// Произведение 2 чисел
		a = Random.Range(40,47);
		b = Random.Range(10,99);
		c = Random.Range(10,99);
		for(int i = 40; i<=47; i++) {
			rand = Random.Range(100,9801);
			text[i].text = rand.ToString();

		}

		rand = b*c;
		text[a].text = rand.ToString();

		//Извлечение квадратного корня
		a = Random.Range(24,31);
		d = Random.Range(10,30);
		for(int i = 24; i<=31; i++) {
			rand = Random.Range(10,40);
			text[i].text = rand.ToString();
			//text[i].color = new Color(0.208f, 0.122f, 0.043f, 1.0f);
		}

		rand = d;
		text[a].text = rand.ToString();

		//Сумма чисел
		a = Random.Range(32,39);
		e = Random.Range(-10000,10000);
		f = Random.Range(-10000,10000);
		for (int i = 32; i<=39; i++) {
			rand = Random.Range(-20000,20000);
			text[i].text = rand.ToString();
			//text[i].color = new Color(0.208f, 0.122f, 0.043f, 1.0f);
		}


		g = e + f;
		rand = g;
		text[a].text = rand.ToString();


		//Квадрат числа
		a = Random.Range(16,23);
		e1 = Random.Range(10,30);
		for (int i = 16; i<=23; i++) {
			rand = Random.Range(100,900);
			text[i].text = rand.ToString(); 
			//text[i].color = new Color(0.208f, 0.122f, 0.043f, 1.0f);
		}
	 
		g1 =e1*e1;
		rand = g1;

		text[a].text = rand.ToString();
	

		//вывод заданий
		zadanie[5].text = "Признак делимости на 5";
		zadanie[0].text = "Признак делимости на 3";
		zadanie[1].text = "Произведение чисел\n"+ b +" и "+ c;
		zadanie[3].text = "Квадратный корень из " + d*d;
		zadanie[4].text = "Сумма чисел\n"+e +" и "+f;
		zadanie [2].text = "Квадрат числа\n" + e1;

		//возвращение переменных в рейкаст для проверки
		player.GetComponent<RayCast> ().b = b;
		player.GetComponent<RayCast> ().c = c;
		player.GetComponent<RayCast> ().d = d;
		player.GetComponent<RayCast> ().e = e;
		player.GetComponent<RayCast> ().e = e1;
		player.GetComponent<RayCast> ().f = f;
		player.GetComponent<RayCast> ().g = g;
		player.GetComponent<RayCast> ().g1 = g1;

	}
	void getBlock() {
		for (int i = 0; i < 6; i++) {
			player.GetComponent<RayCast> ().block [i] = block [i];

		}
	}
}