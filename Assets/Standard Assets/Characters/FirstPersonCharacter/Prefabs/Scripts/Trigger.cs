﻿using UnityEngine;
using System.Collections;


public class Trigger : MonoBehaviour {
	public GameObject ac;
	public GameObject pl;
	public AudioClip dcs;
	private static int s = 2;
	void Start () {
	
	}

	void Update () {
		
	}

	void OnTriggerEnter(Collider col) {
		
		if (col.gameObject.name=="Player"){
			s++;
			pl.GetComponent<RayCast>().setStatusDoors(false);
		
			if (s%2==0) {
			PlaySoundCloseDoor();
			}
		}
		unblockDoor ();
		OffLight ();
		RefreshTextColor();
	}

	public void PlaySoundCloseDoor() {
		ac.GetComponent<AudioSource>().clip = dcs;
		ac.GetComponent<AudioSource>().enabled = true;
		ac.GetComponent<AudioSource>().Play();
	
	}
	void unblockDoor() {
		for (int i = 0; i < 6; i++) {
			pl.GetComponent<RayCast> ().block [i].GetComponent<Animator> ().SetBool("IsBlocked",false);

		}
			
	}

	void OffLight() {
		pl.GetComponent<RayCast>().light.GetComponent<Light>().enabled = false;
		//pl.GetComponent<RayCast>().light.GetComponent<Light>().intensity = 0f;
	}

	void RefreshTextColor(){
		for (int i = 0; i <=47 ; i++) {
			
			pl.GetComponent<RayCast>().cube.GetComponent<WhatIsCube>().text[i].color = new Color(0.208f, 0.122f, 0.043f, 1.0f);
		}
	}

}

	
